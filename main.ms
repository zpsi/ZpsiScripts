#[ZpsiScripts Run on server start procedures.]
#Repository: https://github.com/zpsi/ZpsiScripts

bind(world_changed, null, array(to: 'minigames'), @event,
	sudo('/undisguise '.@event['player'])
)

register_command('watch', array(
	'description': 'Used to watch people.',
	'usage': '/watch <player|off> <reason>',
	'permission': 'z.watch.use',
	'noPermMsg': '&8[ZpsiScripts] &cInsufficient permissions.'
	'tabcompleter': closure()
))

register_command('zhelp', array(
	'description': 'ZpsiScripts help page.',
	'usage': '/zhelp',
	'permission': '*',
))

register_command('zver', array(
	'description': 'ZpsiScripts version.',
	'usage': '/zver',
))

register_command('tr', array(
	'description': 'Formatted broadcast without a prefix.',
	'usage': '/tr <message>',
))

register_command('feed', array(
	'description': 'Feeds a player.',
	'usage': '/feed <player>',
))

register_command('enchantability', array(
	'description': 'Displays a list of possible enchantments for an item.',
	'usage': '/enchantability <item>',
))

register_command('ea', array(
	'description': 'Displays a list of possible enchantments for an item.',
	'usage': '/ea <item>',
))

register_command('start', array(
	'description': 'Teleports the player to a random location if they\'re at /warp wild or at spawn.',
	'usage': '/start',
))

register_command('cmsg', array(
	'description': 'Gets more information about the custom messages.',
	'usage': '/cmsg <help/l|list/l>',
))

set_tabcompleter('watch', closure(@alias, @player, @args, @info) {
    @players = array();
    foreach(@player in all_players()) {
        if(string_position(@player, @args[-1]) == 0) {
            @players[] = @player;
        }
    }
    return(@players);
})

set_tabcompleter('feed', closure(@alias, @player, @args, @info) {
    @players = array();
    foreach(@player in all_players()) {
        if(string_position(@player, @args[-1]) == 0) {
            @players[] = @player;
        }
    }
    return(@players);
})
