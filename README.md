# ZpsiScripts
These scripts are made for the Minecraft Bukkit/Spigot plugin command helper. These scripts are specifically made for the LandOfTheCrafters server (mc.landofthecrafters.com). After a satisfactory script is completed, and it would be noticebly more efficient to convert into a Bukkit/Spigot plugin (Probably around the release of Minecraft 2.0).
Even though this project is tiny, please notify me if this code is used.
Comments and improvements are welcomed.


#Changelog
  1.0 - Started version control
    New _name commands added.
    
  1.0.1 - More edits to useless commands
  
  1.0.2 - Even more edits to useless commands
  
  1.1.0 - Important(ish) update
    Added easy testing
    Big change in cosmetics
  
  1.2.0 - Almost important update
    Added /watch

#Planned Features
/friend
/instanteat or /ie
