#ZpsiScript Procedures/Functions.  See 'aliases.msa' for usage of such procedures.
#Repository: https://github.com/zpsi/ZpsiScripts

proc(_cmsg, @player, @num,
	if(!has_permission(@player, 'z.name') && !has_permission(@player, 'z.cmsg')){
		die(colorize('&8[ZpsiScripts] &cInsufficient permissions.'))
	}
	if(get_value('cmsg_allowed_for_'.to_lower(@player)) == false || get_value('cmsg_allowed_for_all') == false){
		die(colorize('&8[ZpsiScripts] &cSorry, but you have had this ability taken away.'))
	}
	@cmd = split(' ', get_cmd())[0]
	if(@num != ''){
		if(to_lower(@arguments[1]) == 'help' || to_lower(@arguments[1]) == 'h'){
			call_alias('/zmsg help')
			die()
		}
		if(to_lower(@arguments[1]) == 'info' || to_lower(@arguments[1]) == 'i'){
			call_alias('/zmsg info')
			die()
		}
		@nArgs = split(' ', @num)
		if(to_lower(@nArgs[0]) == 'list' || to_lower(@nArgs[0]) == 'l'){
			@pages = integer((array_size(@arguments) - 3)/8) + 1
			if(array_size(@nArgs) > 1){
				if(is_numeric(@nArgs[1])){
					if(@nArgs[1] <= @pages && @nArgs[1] >= 1){
						@page = @nArgs[1]
						@i = 1 + 8*(@page - 1)
					}
					else{
						@page = @pages
						@i = 1 + 8*(@page - 1)
					}
				}
			}
			else{
				@i = 1
				@page = 1
			}			 
			@concatedList = '&bList of possible messages (Page '.@page.'/'.@pages.'):\n'
			for(@i, @i <= @page*8 && @i <= array_size(@arguments) - 2, @i++){
				if(@i < 10){
					@num = '  '.@i
				}
				else{
					@num = ''.@i
				}
				@concatedList = concat(@concatedList, '&8', @num, '.\t&7', @arguments[@i+1])
				if(@i < @page*8 && @i < array_size(@arguments) - 2){
					@concatedList = concat(@concatedList, '\n')
				}
				else{
					if(@pages != 1){
						if(@page < @pages){
							@concatedList = concat(@concatedList, '\n&bDo '.@cmd.' list '.(@page + 1).' to get to the next page.')
						}
						else{
							@concatedList = concat(@concatedList, '\n&bDo '.@cmd.' list '.(@page - 1).' to get to the previous page.')
						}
					}				
				}
			}
			tmsg(@player, colorize(@concatedList))
			die()
		}
		if(@num <= array_size(@arguments) - 1 && @num != 0){
			@msg = @arguments[@num + 1]
		}
		else{
			die(colorize('&8[ZpsiScripts] &cNo messages at that number.'))
		}
	}
	else{
		if(array_size(@arguments) > 3){
			@i = rand(2, array_size(@arguments))
			@msg = @arguments[@i]
		}
		else{
			@msg = @arguments[2]
		}
	}
	
	if(has_value('_nameLimit'.@player) == false){
		store_value('_nameLimit'.@player, true)
	}
	if(get_value('_nameLimit'.@player) == true){
		chatas(@player, colorize(@msg))
	}
	else{
		tmsg(@player, colorize('&8[ZpsiScripts] &cPlease wait at least 8 seconds between uses.'))
	}
	set_timeout(8000, closure(){
		store_value('_nameLimit'.@player, true)
	})
	store_value('_nameLimit'.@player, false)
)

proc(_log, @player, 
	if(@player != '~console'){
		if(!is_null(get_command_block())){
			@a = get_command_block()
			@player = concat('A command block at '.@a[0].' '.@a[1].' '.@a[2])
		}
		console('[ZpsiScripts] '.@player.' just ran '.get_cmd())
	}
)
